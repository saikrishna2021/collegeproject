package com.example.collegeproject.Other;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkAvalability {
    private static NetworkAvalability instance;

    public enum NetworkStatus {
        WIFI,
        MOBILE,
        UNREACHABLE
    }

    private NetworkAvalability() {
    }

    public static NetworkAvalability getInstance() {
        if (instance == null) {
            instance = new NetworkAvalability();
        }
        return instance;
    }



    public static boolean isNetworkConnected(Context context) {
        @SuppressLint("WrongConstant") NetworkInfo activeNetwork = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }
}
