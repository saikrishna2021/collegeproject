package com.example.collegeproject.Other;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.collegeproject.R;

public class Utility {
    public static void show(String text, int color, Context context) {
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService("layout_inflater");
            if (inflater != null) {
                View layout = inflater.inflate(R.layout.toast, null);
                ((TextView) layout.findViewById(R.id.toast_txt)).setText(text);
                layout.setBackgroundColor(color);
                Toast toast = new Toast(context.getApplicationContext());
                toast.setGravity(87, 0, 0);
                toast.setDuration(0);
                toast.setView(layout);
                toast.show();
            }
        } catch (Exception e) {
            Log.d("labelPrinter", e.getMessage());
        }
    }
}
