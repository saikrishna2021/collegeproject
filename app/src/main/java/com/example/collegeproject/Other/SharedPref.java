package com.example.collegeproject.Other;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {


    private static SharedPref mInstance;
    private static Context mCtx;

    private SharedPref(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPref getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPref(context);
        }
        return mInstance;
    }


    public void savePreference(String constantref,String Response) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("User", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(constantref, Response);
        editor.apply();
    }



    public String getPreference(String storedResponse) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("User", Context.MODE_PRIVATE);
        return sharedPreferences.getString(storedResponse, "0");
    }

    public void clearPreference() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("User", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
}
