package com.example.collegeproject.CallBacks;

import com.example.collegeproject.ModelClasses.NewsModel;

import java.util.List;

public interface INewsListener {
        void  onCategorySuccess(List<NewsModel> newsModels);
        void  onCategoryFailed(String message);
}
