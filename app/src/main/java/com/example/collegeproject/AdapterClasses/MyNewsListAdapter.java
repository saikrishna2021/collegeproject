package com.example.collegeproject.AdapterClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.collegeproject.ModelClasses.NewsModel;
import com.example.collegeproject.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyNewsListAdapter extends RecyclerView.Adapter<MyNewsListAdapter.MyViewHolder> {

    Context context;
    List<NewsModel> newsModelList ;

    public MyNewsListAdapter(Context context, List<NewsModel> newsModelList) {
        this.context = context;
        this.newsModelList = newsModelList;
    }


    @NonNull
    @Override
    public MyNewsListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyNewsListAdapter.MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_news_items,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyNewsListAdapter.MyViewHolder holder, int position) {
        if (newsModelList.get(position).getNewsImage() != null) {
            Glide.with(context).load(newsModelList.get(position).getNewsImage()).into(holder.imgNews);
        }
        else
        {
            holder.imgNews.setImageResource(R.drawable.event);
            holder.imgNews.setVisibility(View.GONE);
        }
        if (newsModelList.get(position).getNewsDescription() != null) {
            holder.txtDescription.setText(new StringBuilder(" ").append(newsModelList.get(position).getNewsDescription()));
        }
        else
        {
            holder.txtDescription.setText("");
        }
        if (newsModelList.get(position).getNewsDate() != null) {
            holder.txtDate.setText(new StringBuilder(" ").append(newsModelList.get(position).getNewsDate()));
        }
        else
        {
            holder.txtDate.setText("");
        }
        if (newsModelList.get(position).getNewsTime() != null) {
            holder.txtTime.setText(new StringBuilder(" ").append(newsModelList.get(position).getNewsTime()));
        }
        else
        {
            holder.txtTime.setText("");
        }
        if (newsModelList.get(position).getNewsTitle() != null) {
            holder.txtTitle.setText(new StringBuilder(" ").append(newsModelList.get(position).getNewsTitle()));
        }
        else
        {
            holder.txtTitle.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return newsModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtTitle = itemView.findViewById(R.id.txtTitle) ;
        TextView txtDescription = itemView.findViewById(R.id.txtDescription) ;
        TextView txtDate = itemView.findViewById(R.id.txtDate) ;
        TextView txtTime = itemView.findViewById(R.id.txtTime) ;
        ImageView imgNews = itemView.findViewById(R.id.imgNews);
        CircleImageView imgCirclerDisplay = itemView.findViewById(R.id.imgCirclerDisplay);

        public MyViewHolder(View inflate) {
            super(inflate);

        }
    }
}
