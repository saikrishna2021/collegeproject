package com.example.collegeproject.AdapterClasses;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.collegeproject.R;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.Myholder> {
    private ArrayList<String> values;
    public MyAdapter(ArrayList<String> values) {

        this.values = values;    }

    @NonNull
    @Override
    public Myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new Myholder( LayoutInflater.from( parent.getContext() ).inflate( R.layout.day2, parent, false ) );

    }

    @Override
    public void onBindViewHolder(@NonNull Myholder holder, final int position) {
        holder.t1.setText( values.get( position ) );

    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    class Myholder extends RecyclerView.ViewHolder {
        TextView t1;
        public Myholder(@NonNull View itemView) {
            super( itemView );
            t1=itemView.findViewById( R.id.textView );
        }


    }
}