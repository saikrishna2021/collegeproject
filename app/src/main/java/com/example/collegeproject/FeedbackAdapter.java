package com.example.collegeproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.example.collegeproject.ModelClasses.FeedbackModel;

import java.util.List;

class FeedbackAdapter extends Adapter<FeedbackAdapter.Myholder> {
    Context mContext;
    private List<FeedbackModel> values;

    class Myholder extends ViewHolder {
        TextView t1;

        public Myholder(View itemView) {
            super(itemView);
            this.t1 = (TextView) itemView.findViewById(R.id.textView);
        }
    }

    public FeedbackAdapter(Context mContext2, List<FeedbackModel> values2) {
        this.values = values2;
        this.mContext = mContext2;
    }

    public Myholder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Myholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.displaydata, parent, false));
    }

    public void onBindViewHolder(Myholder holder, int position) {
        holder.t1.setText(((FeedbackModel) this.values.get(position)).getData());
    }

    public int getItemCount() {
        return this.values.size();
    }
}
