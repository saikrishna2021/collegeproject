package com.example.collegeproject.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.example.collegeproject.Other.Utils;
import com.example.collegeproject.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment {

    public ProgressDialog mProgressDialog;
    private FirebaseAuth mAuth;
    public ViewSwitcher mViewSwitcherSignUp;
    Button buttonSignUpNext;
    EditText userName,userEmail,userPassword,userMobile,userConfirmPassword;
    TextInputLayout userNameLayout,userEmailLayout,userPasswordLayout,userMobileLayout,userConfirmPasswordLayout;
    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_sign_up, container, false);
        mAuth = FirebaseAuth.getInstance();
        mViewSwitcherSignUp = (ViewSwitcher) view.findViewById(R.id.viewSwitcherSignUp);
         buttonSignUpNext =  view.findViewById(R.id.buttonSignUpNext);
         userName = view.findViewById(R.id.userName);
         userEmail =  view.findViewById(R.id.userEmail);
         userMobile = view. findViewById(R.id.userMobile);
         userPassword = view. findViewById(R.id.userPassword);
         userConfirmPassword = view. findViewById(R.id.userConfirmPassword);
        buttonSignUpNext =  view.findViewById(R.id.buttonSignUpNext);
        userNameLayout = view.findViewById(R.id.userNameLayout);
        userEmailLayout =  view.findViewById(R.id.userEmailLayout);
        userMobileLayout = view. findViewById(R.id.userMobileLayout);
        userPasswordLayout = view. findViewById(R.id.userPasswordLayout);
        userConfirmPasswordLayout = view. findViewById(R.id.userConfirmPasswordLayout);
        buttonSignUpNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleFirstStepData();
            }
        });

        return view;
    }

    private void handleFirstStepData() {
        String name = userName.getText().toString().trim();
        String email = userEmail.getText().toString().trim();
        String mobileNumber = userMobile.getText().toString().trim();
        String password = userPassword.getText().toString().trim();
        String confirmPassword = userConfirmPassword.getText().toString().trim();
        if (name.length() < 4) {
            userNameLayout.setError("Please enter a valid name");
            return;
        } else {
            userNameLayout.setError(null);
        }
        if (!Utils.isEmailAddressValid(email)) {
            userEmailLayout.setError("Please enter a valid email address");
            return;
        } else
        {
            userEmailLayout.setError(null);
        }
        if (mobileNumber.length() < 10 || mobileNumber.length() > 10) {
            userMobileLayout.setError("Please enter 10 digits mobile number");
            return;
        } else
        {
            userMobileLayout.setError(null);
        }
        if (password.length() < 8 || password.length() > 20) {
            userPasswordLayout.setError("Password min length 8 chars and max length 20 chars");
            return;
        }
        else {
           userPasswordLayout.setError(null);
        }
        if (confirmPassword.length() < 8 || password.length() > 20) {
            userConfirmPasswordLayout.setError("Password min length 8 chars and max length 20 chars");
            return;}
        else
        {
           userConfirmPasswordLayout.setError(null);
        }
        if (!password.equals(confirmPassword)) {
           userConfirmPasswordLayout.setError("Password and Confirm Password must be the same");
            return;  }

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Toast.makeText(getActivity(),"Sucess",Toast.LENGTH_LONG).show();
                            FragmentTransaction transaction    = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.framelayout, new HomeScreen());
                            transaction.commit();

                        } else {
                            // If sign in fails, display a message to the user.

                            Toast.makeText(getActivity(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }
    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }

}
