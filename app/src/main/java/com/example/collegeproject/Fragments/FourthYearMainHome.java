package com.example.collegeproject.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.collegeproject.Activity.CollegeInfomation;
import com.example.collegeproject.Activity.Materials;
import com.example.collegeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FourthYearMainHome extends Fragment {
    Button bt1,bt2,bt3,bt4,bt5,bt6;


    public FourthYearMainHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fourth_year_main_home, container, false);

        bt1 = view.findViewById(R.id.bt_profile);
        bt2 = view.findViewById(R.id.bt_educ);
        bt3 = view.findViewById(R.id.bt_heart);
        bt4 = view.findViewById(R.id.bt_goal);
        bt5 = view.findViewById(R.id.bt_fin);
        bt6 = view.findViewById(R.id.bt_com);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.framelayout, new FourthYearSections());
                transaction.commit();
                transaction.addToBackStack(null);
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Subjects",Toast.LENGTH_SHORT).show();

            }
        });
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction2 = getFragmentManager().beginTransaction();
                transaction2.replace(R.id.framelayout, new AcademicCalender());
                transaction2.commit();
                transaction2.addToBackStack(null);

            }
        });
        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Materials.class);
                startActivity(intent);
            }
        });
        bt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CollegeInfomation.class);
                startActivity(intent);
            }
        });
        bt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction2 = getFragmentManager().beginTransaction();
                transaction2.replace(R.id.framelayout, new NewsFragment());
                transaction2.commit();
                transaction2.addToBackStack(null);
            }
        });
        return view;
    }

}
