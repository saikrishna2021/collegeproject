package com.example.collegeproject.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.collegeproject.Other.NetworkAvalability;
import com.example.collegeproject.Other.Utility;
import com.example.collegeproject.R;
import com.example.collegeproject.Activity.SignUpActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeScreen extends Fragment {
     int count = 0;
    ImageView applogo;

    Button buttonSignUp,buttonSignIn;
    TextView tv;
    public HomeScreen() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_home_screen, container, false);

        buttonSignUp= (Button) view.findViewById(R.id.buttonSignUp);
        buttonSignIn= (Button) view.findViewById(R.id.buttonSignIn);
        tv=view.findViewById(R.id.mar);
        applogo = view.findViewById(R.id.imageViewLogo);
        tv.setSelected(true);
        applogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count++;
                StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(count);
                if (count == 5) {
                    count = 0;
                    startActivity(new Intent(getActivity(), SignUpActivity.class));

                }
            }
        });
        if (!NetworkAvalability.isNetworkConnected(getContext())) {
            Utility.show("No Internet Connection", getContext().getResources().getColor(R.color.redColor), getContext());
        }

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
//               startActivity(new Intent(getActivity(),SignUpActivity.class));
                 FragmentTransaction transaction    = getFragmentManager().beginTransaction();
                 transaction.replace(R.id.framelayout, new SignUpFragment());
                 transaction.addToBackStack(null);
                 transaction.commit();
             }
         });
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(),SignInActivity.class));
                FragmentTransaction transaction    = getFragmentManager().beginTransaction();
                transaction.replace(R.id.framelayout, new SignInFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }



}
