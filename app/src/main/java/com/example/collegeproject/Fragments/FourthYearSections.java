package com.example.collegeproject.Fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.collegeproject.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FourthYearSections extends Fragment {

    Button bt1;
    Button bt2;
    Button bt3;

    public FourthYearSections() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=  inflater.inflate(R.layout.fragment_sections, container, false);
        bt1=view.findViewById(R.id.seca);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction    = getFragmentManager().beginTransaction();
                transaction.replace(R.id.framelayout, new FourthYearWeekFragmenta());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        bt2=view.findViewById(R.id.secb);
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction    = getFragmentManager().beginTransaction();
                transaction.replace(R.id.framelayout, new FourthYearWeekFragmentb());
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });
        bt3=view.findViewById(R.id.secc);
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction    = getFragmentManager().beginTransaction();
                transaction.replace(R.id.framelayout, new FourthYearWeekFragmentc());
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });


        return  view;    }


}