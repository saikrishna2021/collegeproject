package com.example.collegeproject.Fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.collegeproject.Other.Utils;
import com.example.collegeproject.R;
import com.example.collegeproject.Other.SharedPref;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment {
    Button SignIn;
    EditText userEmail,userPassword;
    TextInputLayout userEmailLayout,userPasswordLayout;
    private FirebaseAuth mAuth;

    public SignInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        mAuth = FirebaseAuth.getInstance();
        SignIn = view.findViewById(R.id.buttonSignIn);
        userEmail = view.findViewById(R.id.userEmail);
        userPassword = view.findViewById(R.id.userPassword);
        userEmailLayout = view.findViewById(R.id.userEmailLayout);
        userPasswordLayout = view.findViewById(R.id.userPasswordLayout);
        SignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = userEmail.getText().toString().trim();
                String password = userPassword.getText().toString().trim();
                if (!Utils.isEmailAddressValid(email)) {
                    userEmailLayout.setError("Please enter a valid email address");
                    return;
                } else if (password.length() < 8 || password.length() > 20) {
                    userPasswordLayout.setError("Password min length 8 chars and max length 20 chars");
                    return;
                }
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    SharedPref.getInstance(getContext()).savePreference("Login","1");
                                    Toast.makeText(getActivity(), "Login Sucess",
                                            Toast.LENGTH_SHORT).show();
                                    Toast.makeText(getActivity(),"Sucess",Toast.LENGTH_LONG).show();
                                    FragmentTransaction transaction    = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.framelayout, new Years());
                                    transaction.commit();

                                } else {

                                    Toast.makeText(getContext(), "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();

                                }

                                // ...
                            }
                        });

            }
        });

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }

}
