package com.example.collegeproject.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.collegeproject.ModelClasses.ListItems;
import com.example.collegeproject.R;
import com.example.collegeproject.SimpleAdapterWeek;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondYearWeekFragment  extends Fragment  {
    private static final String URL="https://webpage-6b008.firebaseapp.com/";
     RecyclerView recyclerView;


    SimpleAdapterWeek simpleAdapter;
     List<ListItems> listItems;
    public SecondYearWeekFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_second_year_week, container, false);

        recyclerView =  view.findViewById(R.id.lvMain);


//        String[] title = getResources().getStringArray(R.array.Days);
//        simpleAdapter = new SimpleAdapterWeek( getActivity(),title,this );


        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);


        listItems = new ArrayList<>();

//        // SwipeRefreshLayout
//        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
//        mSwipeRefreshLayout.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener) this);
//        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
//                android.R.color.holo_green_dark,
//                android.R.color.holo_orange_dark,
//                android.R.color.holo_blue_dark);

//        final Handler handler = new Handler();

//        mSwipeRefreshLayout.post(new Runnable() {
//
//            @Override
//            public void run() {
//                mSwipeRefreshLayout.setRefreshing(true);
//                    loadrecyclerviewdata();
//                }
//                // Fetching data from server
//
//        });

        loadrecyclerviewdata();

        deleteCache(getContext());
        return view;
    }

    private void  loadrecyclerviewdata(){

       String URL="https://webpage-6b008.firebaseapp.com/";
       final ProgressDialog progressDialog = new ProgressDialog(getActivity());
       progressDialog.setMessage("Loading Data");
       progressDialog.show();
       StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
           @Override
           public void onResponse(String s) {
               progressDialog.dismiss();

               try {
                   JSONObject jsonObject = new JSONObject(s);
                   JSONArray array = jsonObject.getJSONArray("SecondYearTimeTableA");

                   for (int i = 0;i<array.length();i++)
                   {
                        JSONObject o =array.getJSONObject(i);
                        ListItems items = new ListItems(o.getString("dayname"),o.getString("ids"));
                        listItems.add(items);
                   }
                   simpleAdapter =new SimpleAdapterWeek(getContext(),listItems);
                   recyclerView.setAdapter( simpleAdapter );

               } catch (JSONException e) {
                   e.printStackTrace();
               }

           }
       }, new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {
               progressDialog.dismiss();
               Toast.makeText(getContext(),"Please Connect Internet and Retry",Toast.LENGTH_SHORT).show();

           }
       });
       RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
    requestQueue.add(stringRequest);

    }
    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}
