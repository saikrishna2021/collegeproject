package com.example.collegeproject.Fragments;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.collegeproject.CallBacks.INewsListener;
import com.example.collegeproject.ModelClasses.NewsModel;
import com.example.collegeproject.Other.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class NewsListViewModel  extends ViewModel implements INewsListener {

    private MutableLiveData<List<NewsModel>> newsMutableLiveData;
    private INewsListener newsListener;
    private MutableLiveData<String> messageError =  new MutableLiveData<>();


    public NewsListViewModel() {
        newsListener = this;
    }

    public MutableLiveData<List<NewsModel>> getNewsMutableLiveData() {

        if (newsMutableLiveData == null)
        {
            newsMutableLiveData = new MutableLiveData<>();
            messageError =  new MutableLiveData<>();
            loadNews();

        }
            return newsMutableLiveData;
    }

    private void loadNews() {
        final List<NewsModel> tempList = new ArrayList<>();

        final DatabaseReference newsref  = FirebaseDatabase.getInstance().getReference(Constants.News);
        newsref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        NewsModel model = dataSnapshot1.getValue(NewsModel.class);
                        model.setNewsId(dataSnapshot1.getKey());
                        tempList.add(model);
                    }

                    newsListener.onCategorySuccess(tempList);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                newsListener.onCategoryFailed(databaseError.getMessage());
            }
        });
    }

    public MutableLiveData<String> getMessageError() {
        return messageError;
    }

    @Override
    public void onCategorySuccess(List<NewsModel> newsModels) {
        newsMutableLiveData.setValue(newsModels);
    }

    @Override
    public void onCategoryFailed(String message) {
        messageError.setValue(message);
    }
}
