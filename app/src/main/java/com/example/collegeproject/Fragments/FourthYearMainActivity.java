package com.example.collegeproject.Fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.collegeproject.R;
import com.example.collegeproject.AdapterClasses.SampleAdapterFouth;


/**
 * A simple {@link Fragment} subclass.
 */
public class FourthYearMainActivity extends Fragment  implements SampleAdapterFouth.OnItemClick {

    private RecyclerView recyclerView;
    SampleAdapterFouth sampleAdapter;

    public FourthYearMainActivity() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second_year_main, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.lvMain);
        String[] title = getResources().getStringArray(R.array.Main);
        String[] description = getResources().getStringArray(R.array.Description);
        int image[]={R.drawable.a1,R.drawable.a2,R.drawable.a3,R.drawable.a4};
//        simpleAdapter = new SimpleAdapter( this, title, description,this);
        sampleAdapter = new SampleAdapterFouth( getActivity(),title,description,image, this );
        recyclerView.setAdapter( sampleAdapter );
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }

    @Override
    public void ItemClick(int position) {

    }
}
