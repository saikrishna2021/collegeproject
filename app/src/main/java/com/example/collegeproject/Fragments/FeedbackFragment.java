package com.example.collegeproject.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.collegeproject.ModelClasses.FeedbackModel;
import com.example.collegeproject.Other.NetworkAvalability;
import com.example.collegeproject.Other.Utility;
import com.example.collegeproject.R;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedbackFragment extends Fragment {
    Button button;
    EditText data;
    FeedbackModel feedbackModel;
    TextInputLayout textInputText;

    public FeedbackFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_feedback, container, false);
        textInputText = view.findViewById(R.id.inputfeedback);
        data = view. findViewById(R.id.editfeedback);
        TextView feedback = view. findViewById(R.id.mail);
        feedback.setText(Html.fromHtml("<a href=\"mailto:scet.cse.sra@gmail.com\">scet.cse.sra@gmail.com</a>"));
        feedback.setMovementMethod(LinkMovementMethod.getInstance());
        button = view. findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                boolean isValid = true;
                if (data.getText().toString().isEmpty()) {
                    textInputText.setError("Enter feedback and Submit");
                    isValid = false;
                } else {
                    textInputText.setErrorEnabled(false);
                }
                if (isValid) {
                    if (!NetworkAvalability.isNetworkConnected(getActivity())) {
                        Utility.show("No Internet Connection", getContext().getResources().getColor(R.color.redColor), getActivity());
                    }
                }
                feedbackModel = new FeedbackModel();
                feedbackModel.setData(data.getText().toString());
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("FeedBack");
                myRef.child(myRef.push().getKey())
                        .setValue(data.getText().toString());
                Toast.makeText(getContext(), "Thanks for your feedback", Toast.LENGTH_LONG).show();
                data.setText("");

            }
        });
        return view;
    }
 }

