package com.example.collegeproject.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.collegeproject.AdapterClasses.MyNewsListAdapter;
import com.example.collegeproject.ModelClasses.NewsModel;
import com.example.collegeproject.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {
    RecyclerView recyclerView;
    private NewsListViewModel newsListViewModel;
    MyNewsListAdapter myNewsListAdapter;

    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        newsListViewModel =
                ViewModelProviders.of(this).get(NewsListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_news, container, false);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading Data");
        progressDialog.show();
        recyclerView = root.findViewById(R.id.recycler);
        newsListViewModel.getMessageError().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Toast.makeText(getContext(),"aa"+s,Toast.LENGTH_SHORT).show();
            }
        });

        newsListViewModel.getNewsMutableLiveData().observe(getViewLifecycleOwner(), new Observer<List<NewsModel>>() {
            @Override
            public void onChanged(List<NewsModel> menuCategoryModels) {
                myNewsListAdapter =  new MyNewsListAdapter(getContext(),menuCategoryModels);
                recyclerView.setAdapter(myNewsListAdapter);
                progressDialog.dismiss();
            }
        });
        init();

        return root;
    }

    private void init() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }

}
