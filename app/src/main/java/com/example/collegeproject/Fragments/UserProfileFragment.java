package com.example.collegeproject.Fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.collegeproject.Other.Utils;
import com.example.collegeproject.R;
import com.example.collegeproject.Other.SharedPref;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends Fragment {

    String strUserName;
    EditText etName,etRedgNo,etMobile,etEmail;
    Button update;
    FirebaseFirestore db ;
    ProgressBar progressBar;

    public UserProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View root = inflater.inflate(R.layout.activity_user_profile, container, false);
        etName = root.findViewById(R.id.etName);
        etRedgNo = root.findViewById(R.id.etRedgNo);
        etMobile = root.findViewById(R.id.etMobile);
        etEmail = root.findViewById(R.id.etEmail);
        update = root.findViewById(R.id.update);
        progressBar = root.findViewById(R.id.progressBar);

        strUserName = SharedPref.getInstance(getContext()).getPreference("UserName");
        progressBar.setVisibility(View.VISIBLE);
        db = FirebaseFirestore.getInstance();

        final DocumentReference docRef = db.collection(strUserName).document("Details");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    try {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            etName.setText(String.valueOf(document.get("name")));
                            etEmail.setText(String.valueOf(document.get("email")));
                            etRedgNo.setText(String.valueOf(document.get("redg")));
                            etMobile.setText(String.valueOf(document.get("mobile number")));
                            progressBar.setVisibility(View.GONE);
                        }
                        else {
                            Toast.makeText(getActivity(),"No Data Found",Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                } else {
                    Log.d("asasd", "get failed with ", task.getException());
                }
            }
        });

        styleUiElements();


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String RedgNo = etRedgNo.getText().toString().trim();
                final String name =  etName.getText().toString().trim();
                final String mobileNumber = etMobile.getText().toString().trim();


                Map<String, Object> data = new HashMap<>();
                data.put("name", name);
                data.put("mobile number", mobileNumber);
                data.put("redg",RedgNo);

                DocumentReference documentReference = db.collection(strUserName).document("Details");
                documentReference.update(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getContext(),"Successfully Updated",Toast.LENGTH_SHORT).show();
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getContext(),"Please Try After SomeTime",Toast.LENGTH_SHORT).show();
                            }
                        });



            }
        });

        return root;
    }

    public void styleUiElements()
    {
        etName.setBackgroundResource(R.drawable.settings_edittext_bg);
        etRedgNo.setBackgroundResource(R.drawable.settings_edittext_bg);
        etMobile.setBackgroundResource(R.drawable.settings_edittext_bg);
        etEmail.setBackgroundResource(R.drawable.settings_edittext_bg);

    }

}
