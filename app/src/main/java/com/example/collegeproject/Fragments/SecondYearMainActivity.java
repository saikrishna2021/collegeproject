package com.example.collegeproject.Fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.collegeproject.R;
import com.example.collegeproject.SimpleAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondYearMainActivity extends Fragment implements SimpleAdapter.OnItemClick {
    private RecyclerView recyclerView;
    SimpleAdapter simpleAdapter;
    SwipeRefreshLayout mSwipeRefreshLayout;

    public SecondYearMainActivity() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_second_year_main, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.lvMain);
        String[] title = getResources().getStringArray(R.array.Main);
        String[] description = getResources().getStringArray(R.array.Description);
        int image[]={R.drawable.a1,R.drawable.a2,R.drawable.a3,R.drawable.a4};



        Fragment f=getFragmentManager().getFragments().get(0);
        Log.d(
                "hi","hiii"+f
        );

//        simpleAdapter = new SimpleAdapter( this, title, description,this);
        simpleAdapter = new SimpleAdapter( getActivity(),title,description,image,this );
        recyclerView.setAdapter( simpleAdapter );
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        return view;
    }

    @Override
    public void ItemClick(int position) {
    }
}


