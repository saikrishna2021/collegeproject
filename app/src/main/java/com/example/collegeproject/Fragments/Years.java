package com.example.collegeproject.Fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.collegeproject.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;


/**
 * A simple {@link Fragment} subclass.
 */
public class Years extends Fragment {
Button bt2;
Button bt3;
Button bt4;
int count = 0 ;
    FirebaseStorage storage;
    Bitmap bitmap;
     StorageReference mStorageRef;
    ImageView imageView;
    public Years() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
              View view=  inflater.inflate(R.layout.fragment_years, container, false);
        Log.d("aaa","count"+count);



        try {
            if (count == 0) {
                storage = FirebaseStorage.getInstance();
                mStorageRef = storage.getReferenceFromUrl("gs://college-project-331cc.appspot.com/").child("Banner.jpg");
                final File localFile = File.createTempFile("image", "jpg");
                mStorageRef.getFile(localFile)
                        .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());


                                final Dialog settingsDialog = new Dialog(getActivity());
                                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

                                settingsDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        //nothing;
                                    }
                                });

                                ImageView imageView = new ImageView(getActivity());
                                imageView.setImageBitmap(bitmap);
                                settingsDialog.addContentView(imageView, new RelativeLayout.LayoutParams(
                                        ViewGroup.LayoutParams.WRAP_CONTENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT));
                                settingsDialog.show();
                                final Handler handler = new Handler();
                                final Runnable runnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        if (settingsDialog.isShowing()) {
                                            settingsDialog.dismiss();
                                        }
                                    }
                                };

                                settingsDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        handler.removeCallbacks(runnable);
                                    }
                                });

                                handler.postDelayed(runnable, 5000);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle failed download
                        // ...
                    }
                });
                count++;
            } } catch (IOException e) {
            e.printStackTrace();
        }



      bt2=view.findViewById(R.id.button2);
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction    = getFragmentManager().beginTransaction();
                transaction.replace(R.id.framelayout, new SecondYearMainHome());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        bt3=view.findViewById(R.id.button3);
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction    = getFragmentManager().beginTransaction();
                transaction.replace(R.id.framelayout, new ThirdYearMainHome());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        bt4=view.findViewById(R.id.button4);
        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction    = getFragmentManager().beginTransaction();
                transaction.replace(R.id.framelayout, new FourthYearMainHome());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        return  view;
    }



}
