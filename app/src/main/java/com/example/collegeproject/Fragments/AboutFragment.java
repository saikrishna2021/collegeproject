package com.example.collegeproject.Fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.collegeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {

    FrameLayout settingsMailUs;

    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_about, container, false);

         settingsMailUs = view.findViewById(R.id.settingsMailUs);
        settingsMailUs.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.SENDTO");
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra("android.intent.extra.EMAIL", new String[]{"scet.cse.sra@gmail.com"});
                intent.putExtra("android.intent.extra.SUBJECT", "Email from android app");
                try {
                    AboutFragment.this.startActivity(Intent.createChooser(intent, "Send Email..."));
                } catch (Exception e) {

                }
            }
        });


    return view;
    }

}
