package com.example.collegeproject.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.collegeproject.Other.Utils;
import com.example.collegeproject.R;
import com.example.collegeproject.Other.SharedPref;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignInActivity extends AppCompatActivity {
    Button SignIn,buttonForgotPassword;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        mAuth = FirebaseAuth.getInstance();
        SignIn = findViewById(R.id.buttonSignIn);
        buttonForgotPassword = findViewById(R.id.buttonForgotPassword);
        SignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String email = ((EditText) findViewById(R.id.userEmail)).getText().toString().trim();
                String password = ((EditText) findViewById(R.id.userPassword)).getText().toString().trim();
                if (!Utils.isEmailAddressValid(email)) {
                    ((TextInputLayout) findViewById(R.id.userEmailLayout)).setError("Please enter a valid email address");
                    return;
                } else if (password.length() < 8 || password.length() > 20) {
                    ((TextInputLayout) findViewById(R.id.userPasswordLayout)).setError("Password min length 8 chars and max length 20 chars");
                    return;
                }
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    SharedPref.getInstance(getApplicationContext()).savePreference("Login","1");
                                    SharedPref.getInstance(getApplicationContext()).savePreference("UserName",email);
                                    // Sign in success, update UI with the signed-in user's information
                                    Toast.makeText(SignInActivity.this, "Login Sucess",
                                            Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                                    intent.putExtra("UserName",email);
                                    startActivity(intent);


                                } else {

                                    Toast.makeText(SignInActivity.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();

                                }

                                // ...
                            }
                        });

                }
        });

        buttonForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, ForgetPassword.class));
            }
        });

    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
