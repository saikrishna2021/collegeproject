package com.example.collegeproject.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.example.collegeproject.Other.Utils;
import com.example.collegeproject.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity  {


    String userID;
    Button buttonSignUpNext;
    ProgressBar mProgressDialog;
    public ViewSwitcher mViewSwitcherSignUp;
    FirebaseFirestore db ;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        mViewSwitcherSignUp = (ViewSwitcher) findViewById(R.id.viewSwitcherSignUp);
        mProgressDialog = findViewById(R.id.progressbar);
        buttonSignUpNext =  findViewById(R.id.buttonSignUpNext);
        buttonSignUpNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleFirstStepData();
            }
        });



    }


    private void handleFirstStepData() {
        final String RedgNo = ((EditText) findViewById(R.id.userRedgNo)).getText().toString().trim();
        final String name = ((EditText) findViewById(R.id.userName)).getText().toString().trim();
        final String email = ((EditText) findViewById(R.id.userEmail)).getText().toString().trim();
        final String mobileNumber = ((EditText) findViewById(R.id.userMobile)).getText().toString().trim();
        final String password = ((EditText) findViewById(R.id.userPassword)).getText().toString().trim();
        String confirmPassword = ((EditText) findViewById(R.id.userConfirmPassword)).getText().toString().trim();
        if (name.length() < 4) {
            ((TextInputLayout) findViewById(R.id.userNameLayout)).setError("Please enter a valid name");
                return;
        } else {
            ((TextInputLayout) findViewById(R.id.userNameLayout)).setError(null);
        }
        if (!Utils.isEmailAddressValid(email)) {
            ((TextInputLayout) findViewById(R.id.userEmailLayout)).setError("Please enter a valid email address");
            return;
        } else
        {
            ((TextInputLayout) findViewById(R.id.userEmailLayout)).setError(null);
        }
        if (mobileNumber.length() < 10 || mobileNumber.length() > 10) {
            ((TextInputLayout) findViewById(R.id.userMobileLayout)).setError("Please enter 10 digits mobile number");
            return;
        } else
            {
                ((TextInputLayout) findViewById(R.id.userMobileLayout)).setError(null);
            }
        if (password.length() < 8 || password.length() > 20) {
            ((TextInputLayout) findViewById(R.id.userPasswordLayout)).setError("Password min length 8 chars and max length 20 chars");
            return;
        }
        else {
            ((TextInputLayout) findViewById(R.id.userPasswordLayout)).setError(null);
        }
        if (confirmPassword.length() < 8 || password.length() > 20) {
            ((TextInputLayout) findViewById(R.id.userConfirmPasswordLayout)).setError("Password min length 8 chars and max length 20 chars");
        return;}
        else
            {
                ((TextInputLayout) findViewById(R.id.userConfirmPasswordLayout)).setError(null);
            }
        if (!password.equals(confirmPassword)) {
            ((TextInputLayout) findViewById(R.id.userConfirmPasswordLayout)).setError("Password and Confirm Password must be the same");
      return;  }
        mProgressDialog.setVisibility(View.VISIBLE);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            userID = mAuth.getUid();
                            DocumentReference documentReference = db.collection("users").document(userID);

                            // Create a new user with a first and last name
                            Map<String, Object> user = new HashMap<>();
                            user.put("name", name);
                            user.put("email", email);
                            user.put("mobilenumber", mobileNumber);
                            user.put("password",password);
                            user.put("redg",RedgNo);

                            // Add a new document with a generated ID
//                            db.collection(email)
//                                    .add(user)
//                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                                        @Override
//                                        public void onSuccess(DocumentReference documentReference) {
//                                            mProgressDialog.setVisibility(View.INVISIBLE);
//                                            Toast.makeText(getApplication(),"Sucess",Toast.LENGTH_LONG).show();
//                                            startActivity(new Intent(SignUpActivity.this,LoginScreen.class));
//                                        }
//                                    })
//                                    .addOnFailureListener(new OnFailureListener() {
//                                        @Override
//                                        public void onFailure(@NonNull Exception e) {
//                                            mProgressDialog.setVisibility(View.INVISIBLE);
//                                            Toast.makeText(SignUpActivity.this, "collection data failed.",
//                                                    Toast.LENGTH_SHORT).show();
//                                        }
//                                    });

                            db.collection(email).document("Details").set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    mProgressDialog.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplication(),"Sucess",Toast.LENGTH_LONG).show();

                                    startActivity(new Intent(SignUpActivity.this, LoginScreen.class));
                                }
                            })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            mProgressDialog.setVisibility(View.INVISIBLE);
                                            Toast.makeText(SignUpActivity.this, "collection data failed.",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    });


                        } else {
                            // If sign in fails, display a message to the user.
                            mProgressDialog.setVisibility(View.INVISIBLE);
                            Toast.makeText(SignUpActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }


}
