package com.example.collegeproject.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import com.example.collegeproject.Fragments.AboutFragment;
import com.example.collegeproject.Fragments.FeedbackFragment;
import com.example.collegeproject.Fragments.FourthYearMainHome;
import com.example.collegeproject.Fragments.SecondYearMainHome;
import com.example.collegeproject.Fragments.ThirdYearMainHome;
import com.example.collegeproject.R;

import com.example.collegeproject.Other.SharedPref;

import com.example.collegeproject.Fragments.UserProfileFragment;
import com.example.collegeproject.Fragments.Years;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import static com.example.collegeproject.Other.Constants.TAG_ABOUT;
import static com.example.collegeproject.Other.Constants.TAG_FEEDBACK;
import static com.example.collegeproject.Other.Constants.TAG_FOURTH;
import static com.example.collegeproject.Other.Constants.TAG_HOME;
import static com.example.collegeproject.Other.Constants.TAG_PROFILE;
import static com.example.collegeproject.Other.Constants.TAG_SECOND;
import static com.example.collegeproject.Other.Constants.TAG_THIRD;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView txtUserName;
    public static int navItemIndex = 0;
    public static String CURRENT_TAG = TAG_HOME;
    private Handler mHandler;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        mHandler = new Handler();
        String strUserName = intent.getStringExtra("UserName");
    try {


        if (strUserName == null) {
            strUserName = SharedPref.getInstance(getApplicationContext()).getPreference("UserName");
        }

    }
    catch (Exception e)
    {
        e.printStackTrace();
    }


//        if(!isConnected(MainActivity.this))
//            buildDialog(MainActivity.this).show();
//        else {
//            setContentView(R.layout.activity_main);
//        }
//
//        NotificationCompat.Builder builder=new NotificationCompat.Builder(this);
//        builder.setContentTitle(" WELCOME ");
//        builder.setContentText("");
//
//        builder.setDefaults(NotificationCompat.DEFAULT_ALL);
//        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
//        NotificationManager manager= (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        manager.notify(22,builder.build());


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
         navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        txtUserName = headerView.findViewById(R.id.txtUserName);
        String upperString = strUserName.substring(0, 1).toUpperCase() + strUserName.substring(1);

        txtUserName.setText(upperString);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        txtUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(MainActivity.this,UserProfile.class));
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawers();
                UserProfileFragment UserProfileFragment = new UserProfileFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.framelayout, UserProfileFragment);
                fragmentTransaction.commitAllowingStateLoss();
            }
        });

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadFragment();
        }
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.framelayout);

        Fragment f=getSupportFragmentManager().getFragments().get(0);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else  if( f instanceof Years)
        {
            getSupportActionBar().setTitle("Swarnandhra");
            finish();
            finishAffinity();
        }
        else {
            super.onBackPressed();
        }




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            String url="https://timetable3.webnode.com/our-services/";
            Intent i=new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            i.putExtra("Click Here ",url);
            startActivity(i);
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Fragment fragment=null;


        // Handle navigation view item clicks here.
        int id = item.getItemId();
     if (id == R.id.second)
     {
         navItemIndex = 1;
         CURRENT_TAG = TAG_SECOND;


     }
     else if (id == R.id.third)
     {
         navItemIndex = 2;
         CURRENT_TAG = TAG_THIRD;

     }
     else if (id == R.id.fouth) {

         navItemIndex = 3;
         CURRENT_TAG = TAG_FOURTH;
     }

     else if (id == R.id.college)
     {

                Intent intent = new Intent(MainActivity.this, CollegeInfomation.class);
                startActivity(intent);
     }
     else if (id  == R.id.profile)
     {
         navItemIndex = 4;
         CURRENT_TAG = TAG_PROFILE;

     }
     else if(id == R.id.logout)
     {
//            SharedPref.getInstance(getApplicationContext()).savePreference("UserName","0");
//            SharedPref.getInstance(getApplicationContext()).savePreference("Login","0");
            SharedPref.getInstance(getApplicationContext()).clearPreference();
            startActivity(new Intent(MainActivity.this, LoginScreen.class));
     }
     else if (id == R.id.nav_share)
     {
            Intent i=new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            String shb="https://timetable3.webnode.com/our-services/";
            i.putExtra(Intent.EXTRA_TEXT,shb);
            startActivity(Intent.createChooser(i,"Share using"));
     }
     else if (id == R.id.feedback)
     {
         navItemIndex = 5;
         CURRENT_TAG = TAG_FEEDBACK;
     }
     else if (id == R.id.about)
     {
         navItemIndex = 6;
         CURRENT_TAG = TAG_ABOUT;
     }

        if (item.isChecked()) {
            item.setChecked(false);
        } else {
            item.setChecked(true);
        }
        item.setChecked(true);
     loadFragment();
     DrawerLayout drawer = findViewById(R.id.drawer_layout);
     drawer.closeDrawer(GravityCompat.START);
     return true;
    }

//    private void loadFragment(Fragment fragment) {
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.framelayout, fragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }


    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
        else return false;
        } else
        return false;
    }

//    public AlertDialog.Builder buildDialog(Context c) {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(c);
//        builder.setTitle("No Internet Connection");
//        builder.setMessage("You need to have Internet Connection to access this.");
//
//        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                finish();
//            }
//        });
//
//        return builder;
//    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater menuInflater=getMenuInflater();
//        menuInflater.inflate(R.menu.menu,menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.menu) {
//            Intent i=new Intent(getContext(),MainActivity.class);
//            startActivity(i);
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private Fragment getHomeFragment() {

        switch (navItemIndex) {
            case 0:
                // Home
                Years homeFragment = new Years();
                return homeFragment;
            case 1:

                SecondYearMainHome secondYearMainHome = new SecondYearMainHome();
                return secondYearMainHome;
            case 2:

                ThirdYearMainHome thirdYearMainHome = new ThirdYearMainHome();
                return thirdYearMainHome;
            case 3:
                // Buy Policy fragment
                FourthYearMainHome fourthYearMainHome = new FourthYearMainHome();
                return fourthYearMainHome;
            case 4:
                // ContactUs fragment
                UserProfileFragment userProfileFragment = new UserProfileFragment();
                return userProfileFragment;
            case 5:
                FeedbackFragment feedbackFragment = new FeedbackFragment();
                return feedbackFragment;
            case 6:
                AboutFragment aboutFragment =  new AboutFragment();
                return aboutFragment;
            default:
                return new Years();
        }


    }
    private void loadFragment() {


        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                if(fragment!=null) {
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                            android.R.anim.fade_out);
                    fragmentTransaction.replace(R.id.framelayout, fragment, CURRENT_TAG);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
//                setToolbarTitle();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }


        invalidateOptionsMenu();
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(CURRENT_TAG);
    }


}


