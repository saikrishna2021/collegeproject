package com.example.collegeproject.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.collegeproject.R;
import com.example.collegeproject.Other.SharedPref;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class UserProfile extends AppCompatActivity {
    String strUserName;
    EditText etName,etRedgNo,etMobile,etEmail;
    FirebaseFirestore db ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        etName = findViewById(R.id.etName);
        etRedgNo = findViewById(R.id.etRedgNo);
        etMobile = findViewById(R.id.etMobile);
        etEmail = findViewById(R.id.etEmail);

        strUserName = SharedPref.getInstance(getApplicationContext()).getPreference("UserName");

        db = FirebaseFirestore.getInstance();

        final DocumentReference docRef = db.collection(strUserName).document("Details");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    try {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            etName.setText(String.valueOf(document.get("name")));
                            etEmail.setText(String.valueOf(document.get("email")));
                            etRedgNo.setText(String.valueOf(document.get("redg")));
                            etMobile.setText(String.valueOf(document.get("mobile number")));
                        } else {
                            Toast.makeText(UserProfile.this,"No Data Found",Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                } else {
                    Log.d("asasd", "get failed with ", task.getException());
                }
            }
        });

        styleUiElements();
    }

     public void styleUiElements()
    {
        etName.setBackgroundResource(R.drawable.settings_edittext_bg);
        etRedgNo.setBackgroundResource(R.drawable.settings_edittext_bg);
        etMobile.setBackgroundResource(R.drawable.settings_edittext_bg);
        etEmail.setBackgroundResource(R.drawable.settings_edittext_bg);
    }
}
