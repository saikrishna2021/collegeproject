package com.example.collegeproject.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.collegeproject.Other.NetworkAvalability;
import com.example.collegeproject.R;
import com.example.collegeproject.Other.Utility;

public class LoginScreen extends AppCompatActivity {
    int count = 0;
    ImageView applogo;

    Button buttonSignUp,buttonSignIn;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        buttonSignUp= (Button)findViewById(R.id.buttonSignUp);
        buttonSignIn= (Button)findViewById(R.id.buttonSignIn);
        tv=findViewById(R.id.mar);
        applogo = findViewById(R.id.imageViewLogo);
        tv.setSelected(true);
        applogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count++;
                StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(count);
                if (count == 5) {
                    count = 0;
                    startActivity(new Intent(LoginScreen.this, SignUpActivity.class));

                }
            }
        });
        if (!NetworkAvalability.isNetworkConnected(getApplicationContext())) {
            Utility.show("No Internet Connection", getApplication().getResources().getColor(R.color.redColor), getApplicationContext());
        }

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(LoginScreen.this,SignUpActivity.class));

            }
        });
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginScreen.this, SignInActivity.class));

            }
        });


    }
    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }
}
