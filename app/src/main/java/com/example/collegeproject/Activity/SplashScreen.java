package com.example.collegeproject.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.collegeproject.R;
import com.example.collegeproject.Other.SharedPref;

public class SplashScreen extends AppCompatActivity {
    private int s=2;
    TextView textview1;
    DatePicker picker;
    String Login = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Login = SharedPref.getInstance(getApplicationContext()).getPreference("Login");
//        textview1=findViewById(R.id.textView1);
//        picker=findViewById(R.id.datePicker);
//        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //show the activity in full screen
//        int s=picker.getMonth()+1;
//        textview1.setText(picker.getDayOfMonth()+"/"+(s)+"/"+picker.getYear());

        Logo log=new Logo();
        log.start();
    }
    private class Logo extends Thread{
        public void run() {
            try {
                sleep(1000*s);
            }
            catch (InterruptedException e){
                e.printStackTrace();
            }
        if (Login.contains("1")) {
            Intent i = new Intent(SplashScreen.this, MainActivity.class);
            startActivity(i);
            SplashScreen.this.finish();
        }
        else
        {
            Intent i = new Intent(SplashScreen.this, LoginScreen.class);
            startActivity(i);
            SplashScreen.this.finish();
        }


        }
    }
    }

