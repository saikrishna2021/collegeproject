package com.example.collegeproject.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.collegeproject.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.rengwuxian.materialedittext.MaterialEditText;

public class AdminLogin extends AppCompatActivity {
    FirebaseAuth auth;
    Button bt_login;
    MaterialEditText email;
    MaterialEditText password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);
        setSupportActionBar((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle((CharSequence) "Admin Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        auth = FirebaseAuth.getInstance();
         email = (MaterialEditText) findViewById(R.id.edtAdminEmail);
         password = (MaterialEditText) findViewById(R.id.edtAdminPassword);
         bt_login = (Button) findViewById(R.id.btn_loginAdmin);
         bt_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String text_email = AdminLogin.this.email.getText().toString();
                String text_password = AdminLogin.this.password.getText().toString();
                if (TextUtils.isEmpty(text_email) || TextUtils.isEmpty(text_password)) {
                    Toast.makeText(AdminLogin.this, "All Fields are Required", Toast.LENGTH_SHORT).show();
                } else {
                    AdminLogin.this.auth.signInWithEmailAndPassword(text_email, text_password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        public void onComplete(Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Intent intent = new Intent(AdminLogin.this, CollegeInfomation.class);
                                startActivity(intent);
                                finish();
                                return;
                            }
                            Toast.makeText(AdminLogin.this, "Auth Failed", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }
}
