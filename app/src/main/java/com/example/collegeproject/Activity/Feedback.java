package com.example.collegeproject.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.collegeproject.ModelClasses.FeedbackModel;
import com.example.collegeproject.Other.NetworkAvalability;
import com.example.collegeproject.Other.Utility;
import com.example.collegeproject.R;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Feedback extends AppCompatActivity {
    Button button;
    EditText data;
    FeedbackModel feedbackModel;
    TextInputLayout textInputText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        textInputText = (TextInputLayout) findViewById(R.id.inputfeedback);
        data = (EditText) findViewById(R.id.editfeedback);
        TextView feedback = (TextView) findViewById(R.id.mail);
        feedback.setText(Html.fromHtml("<a href=\"mailto:scet.cse.sra@gmail.com\">scet.cse.sra@gmail.com</a>"));
        feedback.setMovementMethod(LinkMovementMethod.getInstance());
        setSupportActionBar((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbarfeedback));
        getSupportActionBar().setTitle((CharSequence) "FeedBack");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                boolean isValid = true;
                if (data.getText().toString().isEmpty()) {
                    textInputText.setError("Enter feedback and Submit");
                    isValid = false;
                } else {
                    textInputText.setErrorEnabled(false);
                }
                if (isValid) {
                    if (!NetworkAvalability.isNetworkConnected(getApplicationContext())) {
                        Utility.show("No Internet Connection", getApplicationContext().getResources().getColor(R.color.redColor), getApplicationContext());
                    }
                }
                feedbackModel = new FeedbackModel();
                feedbackModel.setData(data.getText().toString());
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("FeedBack");
                myRef.child(myRef.push().getKey())
                .setValue(data.getText().toString());
                Toast.makeText(getApplicationContext(), "Thanks for your feedback", Toast.LENGTH_LONG).show();
                data.setText("");

            }
        });
    }
}
