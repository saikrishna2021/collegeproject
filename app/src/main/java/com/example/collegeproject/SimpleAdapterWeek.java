package com.example.collegeproject;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.example.collegeproject.ModelClasses.ListItems;

import java.util.List;

public class SimpleAdapterWeek  extends RecyclerView.Adapter<SimpleAdapterWeek.Myholder> {
    private SimpleAdapterWeek.OnItemClick itemClick;

    private List<ListItems> listItems;
    Context mContext;
    LayoutInflater layoutInflater;
    TextView title;


    public SimpleAdapterWeek  (Context context, List<ListItems> listItem) {
        this.mContext = context;
        this.listItems = listItem;
        layoutInflater = LayoutInflater.from( context );

    }


    @NonNull
    @Override
    public SimpleAdapterWeek.Myholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater=LayoutInflater.from(mContext);
        View myOwnView= (View) layoutInflater.inflate(R.layout.seconda_week_single_item,viewGroup,false);
        return new SimpleAdapterWeek.Myholder(myOwnView,itemClick);    }

    @Override
    public void onBindViewHolder(@NonNull SimpleAdapterWeek.Myholder myholder, final int i) {
        ListItems listItem = listItems.get(i);
        myholder.tv2.setText(listItem.getTitle());
        myholder.tv3.setText(listItem.getDes());


    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public interface OnItemClick {

        void ItemClick(int position);
    }

    public class Myholder extends RecyclerView.ViewHolder implements AdapterView.OnItemClickListener, View.OnClickListener {
        OnItemClick itemClick;
        Button button;
        TextView tv2;
        TextView tv3;


        public Myholder(@NonNull View itemView, SimpleAdapterWeek.OnItemClick itemClick) {
            super( itemView );

//            button = itemView.findViewById( R.id.button );
            tv2 = itemView.findViewById(R.id.textView2);
            tv3 = itemView.findViewById(R.id.textView3);
            this.itemClick = itemClick;
            itemView.setOnClickListener( this );


        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            itemClick.ItemClick( getAdapterPosition() );
            itemClick.ItemClick( getAdapterPosition() );
        }
    }
}