package com.example.collegeproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.collegeproject.ModelClasses.FeedbackModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DisplayFeedbackData extends AppCompatActivity {
    FeedbackAdapter feedbackAdapter;
    /* access modifiers changed from: private */
    public List<FeedbackModel> mfeedback;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_feedback_data);

        getSupportActionBar().setTitle((CharSequence) "Feedback Data");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        new GetDataFromFirebase().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        mfeedback = new ArrayList();
        readfeedback();
    }

    private void readfeedback() {
        FirebaseDatabase.getInstance().getReference("2b").addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
               mfeedback.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    mfeedback.add((FeedbackModel) snapshot.getValue(FeedbackModel.class));
                }
                feedbackAdapter = new FeedbackAdapter(getApplicationContext(), mfeedback);
               recyclerView.setAdapter(feedbackAdapter);
            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {

            onBackPressed();

        return super.onOptionsItemSelected(item);
    }
}
