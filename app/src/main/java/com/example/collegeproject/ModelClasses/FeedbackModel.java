package com.example.collegeproject.ModelClasses;

public class FeedbackModel {
    private String data;

    public FeedbackModel() {
    }

    public FeedbackModel(String data2) {
        this.data = data2;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data2) {
        this.data = data2;
    }
}
