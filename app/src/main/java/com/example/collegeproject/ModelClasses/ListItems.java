package com.example.collegeproject.ModelClasses;

public class ListItems {

    private String title;
    private String des;
    public ListItems(String title,String des)
    {
        this.title = title;
        this.des = des;
    }

    public String getTitle() {
        return title;
    }



    public String getDes() {
        return des;
    }

}
