package com.example.collegeproject;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.collegeproject.Fragments.AcademicCalender;
import com.example.collegeproject.Fragments.Sections;

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.Myholder> {
    private OnItemClick itemClick;
     Context context;

     Context mContext;
     LayoutInflater layoutInflater;
     TextView title, description;
     String[] titleArray;
     String[] descriptionArray;
     int[] image;
    ImageView imageView;

    public SimpleAdapter(Context context, String[] title, String[] description,int[] images, OnItemClick onclick) {
        this.mContext = context;
        titleArray = title;
        descriptionArray = description;
        image = images;
        layoutInflater = LayoutInflater.from( context );
        itemClick = onclick;

    }


    @NonNull
    @Override
    public Myholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater=LayoutInflater.from(mContext);
        View myOwnView= (View) layoutInflater.inflate(R.layout.mainactivity2_single_item,viewGroup,false);
        return new Myholder(myOwnView,itemClick);    }

    @Override
    public void onBindViewHolder(@NonNull SimpleAdapter.Myholder myholder, final int i) {
        myholder.t1.setText(titleArray[i]);
        myholder.t2.setText( descriptionArray[i] );
        myholder.imageView.setImageResource( image[i] );

        myholder.button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (i)
                {
                    case 0:
                        FragmentTransaction transaction = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.framelayout, new Sections());
                        transaction.commit();
                        transaction.addToBackStack(null);
                        break;

                    case 1:
                        FragmentTransaction transaction1 = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();transaction1.commit();
                        transaction1.addToBackStack(null);
                        break;
                    case 2:
                        FragmentTransaction transaction2 = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                        transaction2.replace(R.id.framelayout, new AcademicCalender());
                        transaction2.commit();
                        transaction2.addToBackStack(null);
                        break;
                }
            }
        } );



    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return titleArray.length;
    }

    public interface OnItemClick {

        void ItemClick(int position);
    }

    public class Myholder extends RecyclerView.ViewHolder implements AdapterView.OnItemClickListener, View.OnClickListener {
        TextView t1;
        TextView t2;
        ImageView imageView;
        OnItemClick itemClick;
        Button button;

        public Myholder(@NonNull View itemView, OnItemClick itemClick) {
            super( itemView );

            t1 = itemView.findViewById( R.id.tvMain );
            t2 = itemView.findViewById(R.id.tvDescription);
            imageView = itemView.findViewById( R.id.ivMain );
            button = itemView.findViewById( R.id.tvClick );
            this.itemClick = itemClick;
            itemView.setOnClickListener( this );


        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            itemClick.ItemClick( getAdapterPosition() );
            itemClick.ItemClick( getAdapterPosition() );
        }
    }
}